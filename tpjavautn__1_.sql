-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2023 a las 03:17:18
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tpjavautn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `razonSoc` varchar(100) NOT NULL,
  `cuit` varchar(100) NOT NULL,
  `telefono` varchar(32) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratocli`
--

CREATE TABLE `contratocli` (
  `id` int(11) NOT NULL,
  `idCli` int(11) NOT NULL,
  `idEspec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE `especialidad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(90) NOT NULL,
  `aplicaciones` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espectec`
--

CREATE TABLE `espectec` (
  `id` int(11) NOT NULL,
  `idTec` int(11) NOT NULL,
  `idEspec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidente`
--

CREATE TABLE `incidente` (
  `id` int(11) NOT NULL,
  `idTec` int(11) DEFAULT NULL,
  `idEspec` int(11) NOT NULL,
  `idCli` int(11) NOT NULL,
  `descripcion` varchar(1001) DEFAULT NULL,
  `tipoProblema` varchar(100) NOT NULL,
  `fecha_Ingreso` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `resuelto` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnico`
--

CREATE TABLE `tecnico` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `telefono` varchar(32) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contratocli`
--
ALTER TABLE `contratocli`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCli` (`idCli`),
  ADD KEY `idEspec` (`idEspec`);

--
-- Indices de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `espectec`
--
ALTER TABLE `espectec`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEspec` (`idEspec`),
  ADD KEY `idTec` (`idTec`);

--
-- Indices de la tabla `incidente`
--
ALTER TABLE `incidente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idTecnico` (`idTec`),
  ADD KEY `idCliente` (`idCli`),
  ADD KEY `idEspec` (`idEspec`);

--
-- Indices de la tabla `tecnico`
--
ALTER TABLE `tecnico`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contratocli`
--
ALTER TABLE `contratocli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `espectec`
--
ALTER TABLE `espectec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `incidente`
--
ALTER TABLE `incidente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tecnico`
--
ALTER TABLE `tecnico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contratocli`
--
ALTER TABLE `contratocli`
  ADD CONSTRAINT `contratocli_ibfk_1` FOREIGN KEY (`idCli`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `contratocli_ibfk_2` FOREIGN KEY (`idEspec`) REFERENCES `especialidad` (`id`);

--
-- Filtros para la tabla `espectec`
--
ALTER TABLE `espectec`
  ADD CONSTRAINT `espectec_ibfk_1` FOREIGN KEY (`idEspec`) REFERENCES `especialidad` (`id`),
  ADD CONSTRAINT `espectec_ibfk_2` FOREIGN KEY (`idTec`) REFERENCES `tecnico` (`id`);

--
-- Filtros para la tabla `incidente`
--
ALTER TABLE `incidente`
  ADD CONSTRAINT `incidente_ibfk_1` FOREIGN KEY (`idTec`) REFERENCES `tecnico` (`id`),
  ADD CONSTRAINT `incidente_ibfk_2` FOREIGN KEY (`idCli`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `incidente_ibfk_3` FOREIGN KEY (`idEspec`) REFERENCES `especialidad` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
